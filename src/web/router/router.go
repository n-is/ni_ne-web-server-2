package router

import (
	"github.com/gorilla/mux"
	"net/http"
	"nine/src/middlewares"
	"nine/src/web/controllers"
)

type route struct {
	Uri     string
	Method  string
	Handler func(w http.ResponseWriter, r *http.Request)
}

var allRoutes = []route{
	{
		Uri:     "/login",
		Method:  http.MethodPost,
		Handler: controllers.Login,
	},
	{
		Uri:     "/login",
		Method:  http.MethodGet,
		Handler: controllers.GetLoginPage,
	},
	{
		Uri:     "/profile_edit",
		Method:  http.MethodGet,
		Handler: controllers.GetProfileEdit,
	},
	{
		Uri:     "/profile_edit",
		Method:  http.MethodPost,
		Handler: controllers.UpdateProfileEdit,
	},
	{
		Uri:     "/profile_read",
		Method:  http.MethodGet,
		Handler: controllers.GetProfileRead,
	},
	{
		Uri:     "/logout",
		Method:  http.MethodGet,
		Handler: controllers.Logout,
	},
	{
		Uri:     "/password_forgot",
		Method:  http.MethodGet,
		Handler: controllers.GetForgotPassword,
	},
	{
		Uri:     "/password_forgot",
		Method:  http.MethodPost,
		Handler: controllers.ForgotPassword,
	},
	{
		Uri:     "/password_new",
		Method:  http.MethodPost,
		Handler: controllers.NewPasswordEnter,
	},
}

func NewRouter() *mux.Router {
	r := mux.NewRouter()

	for _, rt := range allRoutes {
		r.HandleFunc(rt.Uri, middlewares.SetMiddlewareLogger(
			middlewares.SetMiddlewareHTML(
				middlewares.SetMiddlewareNoCache(rt.Handler),
			),
		)).Methods(rt.Method)
	}

	// Add google login and callback route
	r.HandleFunc("/google/login", controllers.GoogleLogin)
	r.HandleFunc("/google/auth", controllers.GoogleLoginCallback)

	r.HandleFunc("/google_profile", controllers.GoogleProfileEdit)

	// Password reset redirect
	r.HandleFunc("/password_reset", controllers.ResetPassword)

	// Declare the static file directory and point it to the asset directory
	staticFileDirectory := http.Dir("src/web/assets/")
	staticFileHandler := http.StripPrefix("/", http.FileServer(staticFileDirectory))
	r.PathPrefix("/").Handler(staticFileHandler)

	return r
}
