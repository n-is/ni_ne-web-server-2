package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"nine/src/config"
	"nine/src/models"
	"sync"
)

var (
	tempUsersMu        sync.Mutex
	tempUsers          map[string]bool
	afterLoginCallback string

	googleCallbackTemplate *template.Template
	logCookiesTemplate     *template.Template
	redirectTemplate       *template.Template
	clearSessionTemplate   *template.Template
)

const (
	minOAuthStringLen  = 50
	maxOAuthStringLen  = 80
	maxRandomStringGen = 10
)

type errorOnly struct {
	Error string `json:"error"`
}

type emailOnly struct {
	Email string `json:"email"`
}

type passwordOnly struct {
	Password string `json:"password"`
}

type idOnly struct {
	ID uint32 `json:"id"`
}

type tokenOnly struct {
	Token string `json:"token"`
}

type tokenCallback struct {
	Token    string
	Callback string
}

func init() {
	tempUsersMu = sync.Mutex{}
	tempUsers = make(map[string]bool)

	googleCallbackTemplate = template.Must(template.New("").Parse(googleTokenPassHTML))
	logCookiesTemplate = template.Must(template.New("").Parse(logCookiesHTML))
	redirectTemplate = template.Must(template.New("").Parse(redirectHTML))
	clearSessionTemplate = template.Must(template.New("").Parse(clearSessionHTML))
}

const clearSessionHTML = `
	<html><body>
	<script>
		sessionStorage.google_auth = ""
		sessionStorage.auth_token = ""
	</script>
	</body></html>
`

const redirectHTML = `
	<html><body>
	<script>
		window.location.href = {{.Callback}}
	</script>
	</body></html>
`

const googleTokenPassHTML = `
	<html><body>
	<script>
		sessionStorage.google_auth = "true"
		sessionStorage.auth_token = {{.Token}}

		window.location.href = {{.Callback}}
	</script>
	</body></html>
`

const logCookiesHTML = `
	<html><body>
	<script>
		valuesToCookies("google", sessionStorage.google_auth)
		valuesToCookies("token", sessionStorage.auth_token)

		function valuesToCookies(k, v) {
			if (v !== undefined && v !== "") {
				document.cookie = k + "=" + v + ";"
			}
		}
	</script>
	</body></html>
`

func SetupAfterLoginCallback(callback string) {
	afterLoginCallback = callback
}

func isNilString(s *string) bool {
	if s == (*string)(nil) {
		return true
	}
	return false
}

func isPassTokenValid(token string) (bool, error) {
	tokenValidUrl := fmt.Sprintf("%s/token/password?token=%s", config.ServerURL, token)
	response, err := http.Get(tokenValidUrl)
	if err != nil {
		log.Println(err)
		return false, err
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK {
		fmt.Println("Contents:", string(contents))
		log.Println("Status:", response.StatusCode)
		return false, err
	}

	tokenValid := models.Token{}
	if err = json.Unmarshal(contents, &tokenValid); err != nil {
		log.Println(err)
		return false, err
	}

	return tokenValid.Valid, nil
}
