package controllers

import (
	"fmt"
	"html/template"
	"net/http"
)

var profileReadPage *template.Template

func init() {
	profileReadPage = template.Must(
		template.New("profile_read.gohtml").ParseFiles("src/web/templates/profile_read.gohtml"),
	)
}

func GetProfileRead(w http.ResponseWriter, r *http.Request) {

	cookie, err := r.Cookie("token")
	if err != nil {
		fmt.Println(err)
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	tok := cookie.Value

	prInfo, err := getUserProfileInfo(tok)
	if err != nil {
		invalidTmpl.Execute(w, invalidInfo{Message: err.Error()})
		return
	}

	profileReadPage.Execute(w, prInfo)
}
