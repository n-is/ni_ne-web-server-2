package controllers

import "net/http"

func Logout(w http.ResponseWriter, r *http.Request) {
	// Clear out the token cookie
	c := http.Cookie{
		Name:   "token",
		Value:  "",
		MaxAge: -1,
	}
	http.SetCookie(w, &c)

	// Clear out the pass token cookie
	c = http.Cookie{
		Name:   "pass_token",
		Value:  "",
		MaxAge: -1,
	}
	http.SetCookie(w, &c)

	// Clear out the google cookie
	c = http.Cookie{
		Name:   "google",
		Value:  "",
		MaxAge: -1,
	}
	http.SetCookie(w, &c)

	clearSessionTemplate.Execute(w, nil)

	// Redirect to login page
	redirectTemplate.Execute(w, tokenCallback{
		Callback: "/login",
	})
}
