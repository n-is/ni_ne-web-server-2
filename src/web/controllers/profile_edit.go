package controllers

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"nine/src/models"
)

var profileEditPage *template.Template

func init() {
	profileEditPage = template.Must(
		template.New("profile_edit.gohtml").ParseFiles("src/web/templates/profile_edit.gohtml"),
	)
}

func GetProfileEdit(w http.ResponseWriter, r *http.Request) {

	cookie, err := r.Cookie("token")
	if err != nil {
		cookies := r.Cookies()
		log.Println(cookies)

		log.Println(err)
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	tok := cookie.Value

	prInfo, err := getUserProfileInfo(tok)
	if err != nil {
		invalidTmpl.Execute(w, invalidInfo{Message: err.Error()})
		return
	}

	if checkGoogleCookie(r) {
		prInfo.ReadOnly = "readonly"
	}

	profileEditPage.Execute(w, prInfo)
}

func UpdateProfileEdit(w http.ResponseWriter, r *http.Request) {

	cookie, err := r.Cookie("token")
	if err != nil {
		http.Redirect(w, r, "/login", http.StatusSeeOther)
		return
	}
	tok := cookie.Value

	err = r.ParseForm()
	if err != nil {
		fmt.Println("Parse Form Error:", err)
		return
	}

	user := models.User{
		Email: r.FormValue("email"),
	}

	name := r.FormValue("name")
	address := r.FormValue("address")
	phone := r.FormValue("phone")

	profile := models.Profile{
		Name:    &name,
		Phone:   &phone,
		Address: &address,
	}

	err = setUser(tok, user)
	if err != nil {
		fmt.Println(err)
	}
	err = setProfile(tok, profile)
	if err != nil {
		fmt.Println(err)
	}

	http.Redirect(w, r, "/profile_read", http.StatusSeeOther)
}

func GoogleProfileEdit(w http.ResponseWriter, r *http.Request) {
	logCookiesTemplate.Execute(w, nil)
	redirectTemplate.Execute(w, tokenCallback{
		Callback: afterLoginCallback,
	})
}
