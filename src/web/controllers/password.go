package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
)

var mailMessage = `
Heard you lost your password? Let us get you into the system now.

Follow the link below to reset your password.
`

var (
	forgotPasswordPage *template.Template
	resetPasswordPage  *template.Template
)

func init() {
	forgotPasswordPage = template.Must(
		template.New("forgot_password.gohtml").ParseFiles("src/web/templates/forgot_password.gohtml"),
	)
	resetPasswordPage = template.Must(
		template.New("reset_password.gohtml").ParseFiles("src/web/templates/reset_password.gohtml"),
	)
}

type passForgotResponse struct {
	EmailError    string
	Error         string
	SendMail      string
	SendMailColor string
}

func GetForgotPassword(w http.ResponseWriter, r *http.Request) {
	forgotPasswordPage.Execute(w, nil)
}

func ForgotPassword(w http.ResponseWriter, r *http.Request) {

	err := r.ParseForm()
	if err != nil {
		forgotPasswordPage.Execute(w, passForgotResponse{
			Error: "Could not parse inputs",
		})
		return
	}

	redirectInfo := models.EmailRedirectInfo{
		Email:    r.FormValue("email"),
		Redirect: config.MailRedirectURL,
		Message:  mailMessage,
		Sender:   config.Sender,
	}
	redirect, _ := json.Marshal(redirectInfo)

	forgotUrl := fmt.Sprintf("%s/password/forgot", config.ServerURL)

	response, err := http.Post(forgotUrl, "application/json", bytes.NewBuffer(redirect))
	if err != nil {
		log.Println(err)
		forgotPasswordPage.Execute(w, passForgotResponse{
			Error: err.Error(),
		})
		return
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK {
		log.Println("Status:", response.StatusCode)
		return
	}

	responseRedirect := models.EmailRedirectInfo{}
	if err = json.Unmarshal(contents, &responseRedirect); err != nil {
		log.Println(err)
		forgotPasswordPage.Execute(w, passForgotResponse{
			Error: err.Error(),
		})
		return
	}
	// Email Sent Successfully
	forgotPasswordPage.Execute(w, passForgotResponse{
		SendMail:      "Mail Sent Successfully",
		SendMailColor: "green",
	})
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	token, err := jwtok.ExtractToken(r)
	if err != nil {
		log.Println(err)
		invalidTmpl.Execute(w, invalidInfo{Message: "Internal Server Error"})
		return
	}

	tokenValid, err := isPassTokenValid(token)
	if err != nil {
		invalidTmpl.Execute(w, invalidInfo{Message: err.Error()})
		return
	}

	if tokenValid {
		// Store the token in the cookie
		c := http.Cookie{
			Name:  "pass_token",
			Value: token,
		}
		http.SetCookie(w, &c)
		fmt.Println("Pass Token Cookie Set")

		// Show the page to enter new password
		resetPasswordPage.Execute(w, nil)
	} else {
		invalidTmpl.Execute(w, invalidInfo{Message: "Token Expired"})
	}
}

func NewPasswordEnter(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Pass Token Cookie Retrieve")
	cookie, err := r.Cookie("pass_token")
	if err != nil {
		log.Println(err)
		invalidTmpl.Execute(w, invalidInfo{Message: "Invalid Cookie"})
		return
	}
	tok := cookie.Value

	tokenValid, err := isPassTokenValid(tok)
	if err != nil {
		invalidTmpl.Execute(w, invalidInfo{Message: err.Error()})
		return
	}
	if !tokenValid {
		invalidTmpl.Execute(w, invalidInfo{Message: "Token Expired"})
		return
	}

	// Parse the form and look at the results
	err = r.ParseForm()
	if err != nil {
		resetPasswordPage.Execute(w, errorOnly{Error: "Couldn't parse inputs"})
		return
	}

	// Check if the passwords match
	passwd := r.FormValue("newPasswd")
	confirm := r.FormValue("confirmPasswd")

	if passwd != confirm {
		resetPasswordPage.Execute(w, errorOnly{Error: "Passwords don't Match"})
		return
	}

	// It is okay to delete the cookie now,
	// so that no one else can access it.
	c := http.Cookie{
		Name:  "pass_token",
		Value: "",
		MaxAge: -1,
	}
	http.SetCookie(w, &c)

	// Now reset the password
	password := passwordOnly{}
	password.Password = passwd
	pass, err := json.Marshal(password)
	if err != nil {
		log.Println(err)
		return
	}

	resetUrl := fmt.Sprintf("%s/password/reset?token=%s", config.ServerURL, tok)

	response, err := http.Post(resetUrl, "application/json", bytes.NewBuffer(pass))
	if err != nil {
		log.Println(err)
		invalidTmpl.Execute(w, invalidInfo{Message: "Unprocessable Entity"})
		return
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK {
		fmt.Println("Contents:", string(contents))
		status := fmt.Sprintf("Status: %d", response.StatusCode)
		log.Println(status)
		invalidTmpl.Execute(w, invalidInfo{Message: status})
		return
	}

	token := tokenOnly{}
	if err = json.Unmarshal(contents, &token); err != nil {
		log.Println(err)
		invalidTmpl.Execute(w, invalidInfo{Message: "Internal Server Error"})
		return
	}

	// Save the token in the cookie
	c = http.Cookie{
		Name:  "token",
		Value: token.Token,
	}
	http.SetCookie(w, &c)

	// Redirect to profile edit page
	http.Redirect(w, r, afterLoginCallback, http.StatusSeeOther)
}
