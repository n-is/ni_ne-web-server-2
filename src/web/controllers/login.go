package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"nine/src/config"
	"nine/src/jwtok"
	"nine/src/models"
	"nine/src/responses"
	"nine/src/utils/random"
)

var loginPage *template.Template

type loginResponse struct {
	Email    string
	Password string
	Error    string
}

func init() {
	loginPage = template.Must(
		template.New("login_page.gohtml").ParseFiles("src/web/templates/login_page.gohtml"),
	)
}

func GetLoginPage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html")
	loginPage.Execute(w, nil)
}

// If user doesn't exist, create a new one
func Login(w http.ResponseWriter, r *http.Request) {

	fmt.Println("Trying to log in")
	w.Header().Set("Content-Type", "text/html")

	err := r.ParseForm()
	if err != nil {
		loginPage.Execute(w, loginResponse{Error: "Could not Parse Inputs"})
		return
	}

	user := models.User{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	body, err := json.Marshal(user)
	if err != nil {
		loginPage.Execute(w, loginResponse{
			Email:    user.Email,
			Password: user.Password,
			Error:    err.Error(),
		})
		return
	}

	loginUrl := fmt.Sprintf("%s/login", config.ServerURL)

	response, err := http.Post(loginUrl, "application/json", bytes.NewBuffer(body))
	if err != nil {
		loginPage.Execute(w, loginResponse{
			Email:    user.Email,
			Password: user.Password,
			Error:    err.Error(),
		})
		return
	}
	defer response.Body.Close()

	token := ""
	// Create new user if not found
	if response.StatusCode == http.StatusNotFound {

		log.Println("Creating User:", user)

		userUrl := fmt.Sprintf("%s/users", config.ServerURL)
		createUser, err := http.Post(userUrl, "application/json", bytes.NewReader(body))
		if err != nil {
			log.Println("Failed to create user")
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		defer createUser.Body.Close()
		contents, _ := ioutil.ReadAll(createUser.Body)
		tok := tokenOnly{}
		err = json.Unmarshal(contents, &tok)
		if err != nil {
			log.Println("Failed to decode user info", err)
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		token = tok.Token

	} else if response.StatusCode < 400 && response.StatusCode >= 200 {

		fmt.Println("User should exist", response.StatusCode)
		responseData, err := ioutil.ReadAll(response.Body)
		if err != nil {
			loginPage.Execute(w, loginResponse{
				Email:    user.Email,
				Password: user.Password,
				Error:    err.Error(),
			})
			return
		}

		tok := tokenOnly{}
		err = json.Unmarshal(responseData, &tok)
		if err != nil {
			loginPage.Execute(w, loginResponse{
				Email:    user.Email,
				Password: user.Password,
				Error:    err.Error(),
			})
			return
		}

		token = tok.Token

	} else {
		// Unauthorized Person
		loginPage.Execute(w, loginResponse{
			Error:    "*unauthorized/mistake password",
		})
		return
	}

	c := http.Cookie{
		Name:  "token",
		Value: token,
	}
	http.SetCookie(w, &c)
	// Redirect to profile edit page
	http.Redirect(w, r, "/profile_edit", http.StatusSeeOther)

	fmt.Println("Logged In:", token)
}

func GoogleLogin(w http.ResponseWriter, r *http.Request) {
	tempUsersMu.Lock()
	defer tempUsersMu.Unlock()

	count := 0
	for {
		randStr := random.String(minOAuthStringLen, maxOAuthStringLen)
		if _, ok := tempUsers[randStr]; !ok {
			tempUsers[randStr] = true
			url := config.GoogleOAuth.AuthCodeURL(randStr)
			http.Redirect(w, r, url, http.StatusTemporaryRedirect)
			break
		}
		count++

		if count >= maxRandomStringGen {
			responses.ERROR(w, http.StatusInternalServerError, errors.New("random string generation failed"))
			break
		}
	}
}

func GoogleLoginCallback(w http.ResponseWriter, r *http.Request) {
	state := r.FormValue("state")

	// This block is just here so that the tempUsers mutex is unlocked
	// as soon as it can.
	{
		tempUsersMu.Lock()
		defer tempUsersMu.Unlock()

		if _, ok := tempUsers[state]; !ok {
			log.Println("invalid oauth state")
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		// It's safe to delete the string now as it's use is over
		delete(tempUsers, state)
	}

	code := r.FormValue("code")
	token, err := config.GoogleOAuth.Exchange(context.TODO(), code)
	if err != nil {
		log.Println("Code exchange failed", err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	if err != nil {
		log.Println("Failed to obtain user info")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	contents, err := ioutil.ReadAll(response.Body)
	response.Body.Close()

	userEmail := emailOnly{}
	err = json.Unmarshal(contents, &userEmail)
	if err != nil {
		log.Println("Failed to decode user info", err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	// Just keep the state as password. This is only for compatibility.
	passwd := state

	infoUrl := fmt.Sprintf("%s/users/email/%s", config.ServerURL, userEmail.Email)
	response, err = http.Get(infoUrl)
	if err != nil {
		log.Println("Failed to obtain user info")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	jwtToken := ""
	if response.StatusCode == http.StatusNotFound {
		info, _ := json.Marshal(map[string]string{
			"email":    userEmail.Email,
			"password": passwd,
		})
		userUrl := fmt.Sprintf("%s/users", config.ServerURL)
		createUser, err := http.Post(userUrl, "application/json", bytes.NewBuffer(info))
		if err != nil {
			log.Println("Failed to create user")
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		defer createUser.Body.Close()
		contents, _ = ioutil.ReadAll(createUser.Body)
		tok := tokenOnly{}
		err = json.Unmarshal(contents, &tok)
		if err != nil {
			log.Println("Failed to decode user info", err)
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}

		jwtToken = tok.Token
	} else {
		contents, _ = ioutil.ReadAll(response.Body)
		userID := idOnly{}

		err = json.Unmarshal(contents, &userID)
		if err != nil {
			log.Println("Failed to decode user info", err)
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}

		jwtToken, _ = jwtok.CreateToken(userID.ID)
	}
	response.Body.Close()

	// Redirect to profile edit page
	googleCallbackTemplate.Execute(w, tokenCallback{
		Token: jwtToken,
		Callback: "/google_profile",
	})
}
