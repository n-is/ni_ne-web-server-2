package controllers

import "html/template"

var invalidTmpl *template.Template

type invalidInfo struct {
	Message string
}

func init() {
	invalidTmpl = template.Must(
		template.New("invalid.gohtml").ParseFiles("src/web/templates/invalid.gohtml"),
	)
}
