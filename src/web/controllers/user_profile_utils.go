package controllers

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"nine/src/config"
	"nine/src/models"
)

type userProfileInfo struct {
	Name       string
	Address    string
	Phone      string
	Email      string
	ReadOnly   string
	PhoneError string
	EmailError string
}

func getUser(token string) (models.User, error) {

	getUserUrl := fmt.Sprintf("%s/users?token=%s", config.ServerURL, token)

	response, err := http.Get(getUserUrl)
	if err != nil {
		log.Println(err)
		return models.User{}, err
	}
	defer response.Body.Close()

	contents, _ := ioutil.ReadAll(response.Body)

	user := models.User{}
	err = json.Unmarshal(contents, &user)
	if err != nil {
		log.Println(err)
		return models.User{}, err
	}

	return user, nil
}

func getProfile(token string) (models.Profile, error) {

	getProfilePage := fmt.Sprintf("%s/users/profile?token=%s", config.ServerURL, token)

	response, err := http.Get(getProfilePage)
	if err != nil {
		log.Println(err)
		return models.Profile{}, err
	}
	defer response.Body.Close()

	if response.StatusCode == http.StatusNoContent {
		return models.Profile{}, nil
	}

	contents, _ := ioutil.ReadAll(response.Body)

	var profile models.Profile
	err = json.Unmarshal(contents, &profile)
	if err != nil {
		log.Println(err)
		return models.Profile{}, err
	}

	return profile, nil
}

func getUserProfileInfo(token string) (userProfileInfo, error) {

	profile, err := getProfile(token)
	if err != nil {
		return userProfileInfo{}, err
	}

	user, err := getUser(token)
	if err != nil {
		return userProfileInfo{}, err
	}

	var prInfo userProfileInfo
	if !isNilString(profile.Name) {
		prInfo.Name = *profile.Name
	}
	if !isNilString(profile.Address) {
		prInfo.Address = *profile.Address
	}
	if !isNilString(profile.Phone) {
		prInfo.Phone = *profile.Phone
	}
	prInfo.Email = user.Email

	return prInfo, nil
}

func setUser(token string, user models.User) error {
	client := http.Client{}

	email := emailOnly{
		Email: user.Email,
	}
	contents, err := json.Marshal(email)
	if err != nil {
		return err
	}

	updateProfilePage := fmt.Sprintf("%s/users?token=%s", config.ServerURL, token)

	fmt.Println("Updating user info")
	req, err := http.NewRequest(http.MethodPut, updateProfilePage, bytes.NewReader(contents))
	if err != nil {
		return err
	}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return errors.New(fmt.Sprintf("Status Code: %d", resp.StatusCode))
	}

	return nil
}

func setProfile(token string, profile models.Profile) error {

	client := http.Client{}

	body, err := json.Marshal(profile)
	if err != nil {
		log.Println(err)
		return err
	}

	profileUrl := fmt.Sprintf("%s/users/profile?token=%s", config.ServerURL, token)
	getReq, err := http.Get(profileUrl)
	if err != nil {
		log.Println(err)
		return err
	}
	log.Println("Profile Get Status Code:", getReq.StatusCode)

	var req *http.Request

	if getReq.StatusCode == http.StatusNoContent {
		req, err = http.NewRequest(http.MethodPost, profileUrl, bytes.NewReader(body))
		if err != nil {
			log.Println(err)
			return err
		}

	} else {
		req, err = http.NewRequest(http.MethodPut, profileUrl, bytes.NewReader(body))
		if err != nil {
			log.Println(err)
			return err
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Println(err)
		return err
	}
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return errors.New(fmt.Sprintf("Status Code: %d", resp.StatusCode))
	}

	return nil
}

func checkGoogleCookie(r *http.Request) bool {
	cookie, err := r.Cookie("google")
	if err == nil {
		val := cookie.Value
		if val == "true" {
			return true
		}
	}

	return false
}
