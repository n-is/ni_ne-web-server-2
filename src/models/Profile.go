package models

import "time"

type Profile struct {
	ID   uint32 `json:"id"`

	Name    *string `json:"name"`
	Phone   *string `json:"phone"`
	Address *string `json:"address"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
