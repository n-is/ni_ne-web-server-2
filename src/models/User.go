package models

import (
	"time"
)

type User struct {
	ID       uint32 `json:"id"`
	Email    string `json:"email"`
	Password string `json:"password,omitempty"`

	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}
