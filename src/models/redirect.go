package models

// EmailRedirectInfo defines the necessary elements required
// by the api-server to send the email.
type EmailRedirectInfo struct {
	Email    string `json:"email"`
	Redirect string `json:"redirect"`
	Message  string `json:"message"`
	Sender   string `json:"sender"`
}
